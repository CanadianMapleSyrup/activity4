package Activity4.activity4.inheritance;

public class BookStore {
    public static void main(String[] args) {
        Book[] books = new Book[5];
        books[0] = new Book("Lord of the Rings 1","Tolkien");
        books[1] = new ElectronicBook("Lord of the Rings 2","Tolkien",1024);
        books[2] = new Book("Lord of the Rings 3","Tolkien");
        books[3] = new ElectronicBook("Hobbit 1","Tolkien",1500);
        books[4] = new ElectronicBook("Dobbit 1","Not Tolkien",1501);

        for (Book b : books) {
            System.out.println(b);
        }
    }
}
